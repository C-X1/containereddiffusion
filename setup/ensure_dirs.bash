#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_PATH=$SCRIPT_PATH/..
INSTALL_PATH=$PROJECT_PATH/install/automatic1111

mkdir $INSTALL_PATH
cd $INSTALL_PATH # Goto install dir

LOWER=$INSTALL_PATH/repo
UPPER=$INSTALL_PATH/install
WORK=$INSTALL_PATH/.overlay_work_dir

mkdir -p $WORK
mkdir $INSTALL_PATH/repo

# Checkout stable-diffusion-webui
git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui/ $INSTALL_PATH/repo

# Create overlay for installation changes
mkdir -p install

#Create User overlays
mkdir -p \
    $PROJECT_PATH/user/default/embeddings \
    $PROJECT_PATH/user/default/extensions \
    $PROJECT_PATH/user/default/poses \
    $PROJECT_PATH/user/default/models \
    $PROJECT_PATH/user/default/wildcards

#Create User overlay for additional models
mkdir -p $PROJECT_PATH/user/additional

cd $PROJECT_PATH
