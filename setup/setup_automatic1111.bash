#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_PATH=$SCRIPT_PATH/..
INSTALL_PATH=$PROJECT_PATH/install/automatic1111

bash $PROJECT_PATH/setup/ensure_dirs.bash

LOWER=$INSTALL_PATH/repo
UPPER=$INSTALL_PATH/install
WORK=$INSTALL_PATH/.overlay_work_dir

docker run -it \
       --rm \
       --name stablediff_installation \
       --gpus all \
       --tmpfs /tmp \
       --env HOME=/etc/automatic1111 \
       --hostname stable_installation \
       -p 7860:7860 \
       --mount type=volume,dst=/etc/automatic1111,volume-driver=local,volume-opt=type=overlay,\"volume-opt=o=lowerdir=$LOWER,upperdir=$UPPER,workdir=$WORK\",volume-opt=device=overlay \
       -w /etc/automatic1111 \
       -v $PROJECT_PATH/docker/entry_install_automate1111.bash:/entry.bash:ro \
       --entrypoint /entry.bash \
       ai_container
