#!/bin/bash

function overlay_state {
   OVERLAY_STATE=$(mount | grep 'tmpfs on /tmp/stable')
}


SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_PATH=$SCRIPT_PATH/..
INSTALL_PATH=$PROJECT_PATH/install/automatic1111

bash $PROJECT_PATH/setup/ensure_dirs.bash

LOWER_A1111=$INSTALL_PATH/repo:$INSTALL_PATH/install:$PROJECT_PATH/user/default:$PROJECT_PATH/user/additional

WORK_A1111=/tmp/stable/automatic1111_work #HOST DIR
UPPER_A1111=/tmp/stable/automatic1111_upper #HOST DIR

overlay_state
if [ -z "$OVERLAY_STATE" ]; then
    echo "Creating overlay tmpfs directories"
    mkdir -p /tmp/stable
    sudo mount -t tmpfs tmpfs /tmp/stable
    mkdir -p $WORK
    mkdir -p $UPPER
fi
overlay_state
if [ -z "$OVERLAY_STATE" ]; then
    echo "MISSING OVERLAY DIR"
    exit 1
fi

docker run -it \
       --rm \
       --name stablediff_installation \
       --gpus all \
       --tmpfs /tmp \
       --env HOME=/etc/automatic1111 \
       --hostname stable_installation \
       -p 7860:7860 \
       --mount type=volume,dst=/etc/automatic1111,volume-driver=local,volume-opt=type=overlay,\"volume-opt=o=lowerdir=$LOWER_A1111,upperdir=$UPPER_A1111,workdir=$WORK_A1111\",volume-opt=device=overlay \
       -w /etc/automatic1111 \
       -v $PROJECT_PATH/docker/entry_install_automate1111.bash:/entry.bash:ro \
       --entrypoint /entry.bash \
       ai_container
